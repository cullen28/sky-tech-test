# Product Selection | Coding Test

## The Overview

We would like you to create a product selection page that allows a user to choose from a series of products that we offer. We then want the user to be able to add and remove those products from the basket. The checkout button doesn't need to do anything except log the current basket contents to the console.
Below is a UX wireframe of what we would like the interface to look like:
![image](https://user-images.githubusercontent.com/14904083/47558924-2649b600-d90c-11e8-8120-34691f284276.png) This is just a very basic starter idea - you're welcome to take the concept as far as you want and use the opportunity to show us your creativity. 

You're free to theme the site how you want, and use any additional libraries. We're not looking for the most on-brand sample app, but clear knowledge of React and CSS with good practice demonstrated. Our frontends are built with React and a styled-components backed UI toolkit.

## Application Startup Guide

A barebones webpack server has been setup alongside an `app.js` within `/src`. What else happens within the remit of `/src` is entirely up to you. All the data for the products we want you to display can be obtained from `http://localhost:8080/data/products.json`.

### Scripts

To run this project you'll need to run the follow commands:
- [if you have nvm installed] `nvm use`
- `yarn install`
- `yarn dev`
- Navigate to localhost:8080/app in your browser

### Tests 
Jest has been configured on this app. To run you can use:

`npm run test` or `yarn test`

Tests are not mandatory, but you're welcome to write them if it suits your development style. 


