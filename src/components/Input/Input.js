import React from 'react'
import PropTypes from 'prop-types'

import { useField } from 'formik'

import Text from '../Text/Text'

import inputStyles from '../../styles/components/_input.module.scss'

const Input = props => {
  const { name, id, type, label } = props

  const [field] = useField(props)

  return (
    <label className={inputStyles.input} htmlFor={id || name}>
      <input id={id || name} name={name} type={type} {...field} />
      <Text size="large">
        <strong>{label}</strong>
      </Text>
    </label>
  )
}

Input.defaultProps = {
  id: '',
}

Input.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text', 'checkbox', 'radio']).isRequired,
  label: PropTypes.string.isRequired,
}

export default Input
