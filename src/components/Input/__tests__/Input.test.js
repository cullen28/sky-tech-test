import React from 'react'

import { render } from '@testing-library/react'

import { Formik, Form } from 'formik'

import Input from '../Input'

describe('<Input>', () => {
  it('should output label, name, type=checkbox and id', () => {
    const { getByText } = render(
      <Formik initialValues={{}} onSubmit={() => jest.fn()}>
        <Form>
          <Input label="Label" name="group" type="checkbox" id="test" />,
        </Form>
      </Formik>,
    )

    getByText('Label')
    getByText('', {
      selector: 'input[id="test"][name="group"][type="checkbox"]',
    })
  })
})
