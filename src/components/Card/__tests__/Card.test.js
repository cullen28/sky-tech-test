import React from 'react'

import { render } from '@testing-library/react'

import Card from '../Card'

describe('<Card>', () => {
  it('should output text passed in as children', () => {
    const { getByText } = render(<Card>Hello</Card>)

    getByText('Hello', { selector: '.card' })
  })
})
