import React from 'react'
import PropTypes from 'prop-types'

import cardStyles from '../../styles/components/_card.module.scss'

const Card = ({ children }) => <div className={cardStyles.card}>{children}</div>

Card.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Card
