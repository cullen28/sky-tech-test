import React from 'react'
import PropTypes from 'prop-types'

import classnames from 'classnames'

import textStyles from '../../styles/components/_text.module.scss'
import typographyStyles from '../../styles/global/_typography.module.scss'

const Text = ({ element, size, align, colour, dataTestId, children }) => {
  const Element = element

  return (
    <Element
      className={classnames(
        textStyles.text,
        textStyles[size],
        typographyStyles[colour],
        typographyStyles[align],
      )}
      data-test-id={dataTestId}
    >
      {children}
    </Element>
  )
}

Text.defaultProps = {
  element: 'p',
  size: 'medium',
  align: null,
  colour: 'grey',
  dataTestId: '',
}

Text.propTypes = {
  element: PropTypes.oneOf(['p', 'span']),
  size: PropTypes.oneOf(['small', 'medium', 'large', 'xLarge']),
  colour: PropTypes.oneOf(['grey', 'white', 'black', 'fancy']),
  align: PropTypes.oneOf(['left', 'center', 'right']),
  dataTestId: PropTypes.string,
  children: PropTypes.node.isRequired,
}

export default Text
