import React from 'react'

import { render } from '@testing-library/react'

import Text from '../Text'

describe('<Text>', () => {
  it('should output text passed in as children as p element', () => {
    const { getByText } = render(<Text>Hello</Text>)

    getByText('Hello', { selector: 'p' })
  })

  it('should output as span element and grey colour', () => {
    const { getByText } = render(
      <Text element="span" colour="grey">
        Hello
      </Text>,
    )

    getByText('Hello', { selector: 'span.grey' })
  })

  it('should output as right aligned and small', () => {
    const { getByText } = render(
      <Text align="right" size="small">
        Hello
      </Text>,
    )

    getByText('Hello', { selector: '.right.small' })
  })
})
