import React from 'react'
import PropTypes from 'prop-types'

import classnames from 'classnames'

import buttonStyles from '../../styles/components/_button.module.scss'

const Button = ({ type, shouldSubmit, isDisabled, children }) => (
  <button
    type={shouldSubmit ? 'submit' : 'button'}
    className={classnames(buttonStyles.button, buttonStyles[type])}
    disabled={isDisabled}
  >
    {children}
  </button>
)

Button.defaultProps = {
  type: 'primary',
  shouldSubmit: false,
  isDisabled: false,
}

Button.propTypes = {
  type: PropTypes.oneOf(['primary', 'secondary']),
  shouldSubmit: PropTypes.bool,
  isDisabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
}

export default Button
