import React from 'react'

import { render } from '@testing-library/react'

import Button from '../Button'

describe('<Button>', () => {
  it('should output text passed in as children', () => {
    const { getByText } = render(<Button>Hello</Button>)

    getByText('Hello', { selector: '.button' })
  })

  it('should output Button with disabled attribute and type submit', () => {
    const { getByText } = render(
      <Button isDisabled shouldSubmit>
        Hello
      </Button>,
    )

    getByText('Hello', { selector: '[disabled][type="submit"]' })
  })
})
