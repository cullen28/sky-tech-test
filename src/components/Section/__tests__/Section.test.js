import React from 'react'

import { render } from '@testing-library/react'

import Section from '../Section'

describe('<Section>', () => {
  it('should output text passed in as children', () => {
    const { getByText } = render(<Section>Hello</Section>)

    getByText('Hello', { selector: '.section' })
  })
})
