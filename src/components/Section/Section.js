import React from 'react'

import PropTypes from 'prop-types'

import sectionStyles from '../../styles/components/_section.module.scss'

const Section = ({ children }) => (
  <section className={sectionStyles.section}>{children}</section>
)

Section.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Section
