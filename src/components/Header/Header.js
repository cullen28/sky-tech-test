import React from 'react'

import { Link } from '@reach/router'

import headerStyles from '../../styles/components/_header.module.scss'
import globalStyles from '../../styles/global/_global.module.scss'

const Header = () => (
  <header className={headerStyles.header}>
    <div className={globalStyles.border} />

    <div className={headerStyles.wrapper}>
      <Link to="/" title="Homepage">
        <img
          className={headerStyles.logo}
          src="https://www.sky.com/assets/masthead/images/sky-logo.png"
          alt="Sky logo"
        />
      </Link>

      <Link to="/">TV</Link>
      <Link to="/">Sky Q</Link>
      <Link to="/">Broadband</Link>
      <Link to="/">Mobile</Link>
      <Link to="/">Deals</Link>
    </div>

    <div className={headerStyles.offerSection}>
      <p>
        Our best deals on TV, Broadband and Mobile |{' '}
        <strong>See all deals &gt;</strong>
      </p>
    </div>
  </header>
)

export default Header
