import React from 'react'

import { Link } from '@reach/router'

import footerStyles from '../../styles/components/_footer.module.scss'
import globalStyles from '../../styles/global/_global.module.scss'

const Footer = () => (
  <footer className={footerStyles.footer}>
    <div className={footerStyles.wrapper}>
      <Link to="/" title="Homepage">
        <img
          className={footerStyles.logo}
          src="https://www.sky.com/assets/masthead/images/sky-logo.png"
          alt="Sky logo"
        />
      </Link>

      <p>© 2021 Sky UK</p>
    </div>

    <div className={globalStyles.border} />
  </footer>
)

export default Footer
