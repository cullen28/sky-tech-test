import React from 'react'
import PropTypes from 'prop-types'

import classnames from 'classnames'

import headingStyles from '../../styles/components/_heading.module.scss'
import typographyStyles from '../../styles/global/_typography.module.scss'

const Heading = ({ level, align, colour, children }) => {
  const Element = `h${level}`

  return (
    <Element
      className={classnames(
        headingStyles.heading,
        headingStyles[`h${level}`],
        typographyStyles[colour],
        typographyStyles[align],
      )}
    >
      {children}
    </Element>
  )
}

Heading.defaultProps = {
  align: null,
  colour: 'grey',
}

Heading.propTypes = {
  level: PropTypes.oneOf([1, 2, 3, 4, 5, 6]).isRequired,
  colour: PropTypes.oneOf(['grey', 'white', 'black', 'fancy']),
  align: PropTypes.oneOf(['left', 'center', 'right']),
  children: PropTypes.node.isRequired,
}

export default Heading
