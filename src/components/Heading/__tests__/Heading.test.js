import React from 'react'

import { render } from '@testing-library/react'

import Heading from '../Heading'

describe('<Heading>', () => {
  it('should output text passed in as children as h2 element', () => {
    const { getByText } = render(<Heading level={2}>Hello</Heading>)

    getByText('Hello', { selector: 'h2.heading' })
  })

  it('should output as h1 element, fancy colour and center aligned', () => {
    const { getByText } = render(
      <Heading level={1} colour="fancy" align="center">
        Hello
      </Heading>,
    )

    getByText('Hello', { selector: 'h1.fancy.center' })
  })
})
