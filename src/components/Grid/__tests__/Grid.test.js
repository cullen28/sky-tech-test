import React from 'react'

import { render } from '@testing-library/react'

import Grid from '../Grid'

describe('<Grid>', () => {
  it('should output text passed in as children and with column class', () => {
    const { getByText } = render(<Grid>Hello</Grid>)

    getByText('Hello', { selector: '.grid.column' })
  })

  it('should output Grid with row class', () => {
    const { getByText } = render(<Grid isRow>Hello</Grid>)

    getByText('Hello', { selector: '.grid.row' })
  })

  it('should output responsive size classes', () => {
    const { getByText } = render(
      <Grid xs={12} sm={3} md={4} lg={6} xl={6}>
        Hello
      </Grid>,
    )

    getByText('Hello', { selector: '.xs-12.sm-3.md-4.lg-6.xl-6' })
  })
})
