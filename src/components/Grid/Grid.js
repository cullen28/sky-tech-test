import React from 'react'
import PropTypes from 'prop-types'

import classnames from 'classnames'

import gridStyles from '../../styles/components/_grid.module.scss'

// NOTE: for the purposes of this tech test I'm just outputting Grid with only a 3 column / 4 section layout
const Grid = ({ xs, sm, md, lg, xl, isRow, children }) => (
  <div
    className={classnames(
      gridStyles.grid,
      gridStyles[`xs-${xs}`],
      gridStyles[`sm-${sm}`],
      gridStyles[`md-${md}`],
      gridStyles[`lg-${lg}`],
      gridStyles[`xl-${xl}`],
      isRow ? gridStyles.row : gridStyles.column,
    )}
  >
    {children}
  </div>
)

Grid.defaultProps = {
  xs: null,
  sm: null,
  md: null,
  lg: null,
  xl: null,
  isRow: false,
}

Grid.propTypes = {
  xs: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  sm: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  md: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  lg: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  xl: PropTypes.oneOf([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]),
  isRow: PropTypes.bool,
  children: PropTypes.node.isRequired,
}

export default Grid
