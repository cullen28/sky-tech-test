import * as api from './'

export const PRODUCTS = 'http://localhost:8080/data/products.json'

export const getProducts = () =>
  new Promise((resolve, reject) =>
    api
      .get(PRODUCTS)
      .then(res => resolve(res))
      .catch(err => reject(err)),
  )
