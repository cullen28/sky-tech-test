import * as axios from 'axios'

export const get = endpoint =>
  new Promise((resolve, reject) =>
    axios
      .request({
        method: 'get',
        url: endpoint,
        responseType: 'json',
      })
      .then(response => resolve(response.data))
      .catch(error => reject(error)),
  )
