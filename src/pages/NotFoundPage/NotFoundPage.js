import React from 'react'

import Section from '../../components/Section/Section'
import Heading from '../../components/Heading/Heading'

const NotFoundPage = () => (
  <Section>
    <Heading level={1} colour="fancy">
      404 page not found
    </Heading>
  </Section>
)

export default NotFoundPage
