import React from 'react'

import { render } from '@testing-library/react'

import NotFoundPage from '../NotFoundPage'

describe('<NotFoundPage>', () => {
  it('should match snapshot', () => {
    const { getByText, container } = render(<NotFoundPage />)

    getByText('404 page not found')
    expect(container.firstChild).toMatchSnapshot()
  })
})
