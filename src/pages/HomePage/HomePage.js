import React, { useEffect, useState } from 'react'

import { getProducts } from '../../api/products'

import { Form, Formik } from 'formik'

import Section from '../../components/Section/Section'
import Heading from '../../components/Heading/Heading'
import Card from '../../components/Card/Card'
import Text from '../../components/Text/Text'
import Grid from '../../components/Grid/Grid'
import Button from '../../components/Button/Button'
import Input from '../../components/Input/Input'

const HomePage = () => {
  const [products, setProducts] = useState({ sports: [], news: [] })

  const [basket, setBasket] = useState({ sports: [], news: [], total: 0 })

  const updateBasket = values => {
    let total = 0

    products.sports.forEach(
      sport =>
        values.sports.includes(sport.name) &&
        (total = total + Number(sport.monthlyCost)),
    )

    products.news.forEach(
      news =>
        values.news.includes(news.name) &&
        (total = total + Number(news.monthlyCost)),
    )

    setBasket({ ...values, total })
  }

  useEffect(() => {
    const getData = async () => {
      const data = await getProducts()

      const sportsProducts = data.filter(
        product => product.category === 'Sport',
      )
      const newsProducts = data.filter(product => product.category === 'News')

      setProducts({ sports: sportsProducts, news: newsProducts })
    }

    getData()
  }, [])

  return (
    <Section>
      <Heading level={1} align="center" colour="fancy">
        Build your Sky TV package
      </Heading>

      <Formik
        initialValues={{ sports: [], news: [] }}
        onSubmit={values => console.log(values)}
        validate={updateBasket}
      >
        <Form>
          <Grid isRow>
            <Grid xs={12} sm={4}>
              <Card>
                <Heading level={3} colour="fancy">
                  Sports
                </Heading>
                <Text size="large">Add your favourite sports channels</Text>

                {products.sports.map((sport, i) => (
                  <div key={sport.name}>
                    <hr />

                    <Input
                      type="checkbox"
                      name="sports"
                      id={sport.name}
                      label={sport.name}
                      value={sport.name}
                    />
                    <Text size="small">{sport.description}</Text>
                    <Text size="xLarge" colour="fancy">
                      £{sport.monthlyCost}{' '}
                      <Text size="small" element="span" colour="grey">
                        per month
                      </Text>
                    </Text>
                    <Text size="small">
                      Contract term {sport.contractLength} months
                    </Text>

                    {i + 1 === products.sports.length && <hr />}
                  </div>
                ))}
              </Card>
            </Grid>

            <Grid xs={12} sm={4}>
              <Card>
                <Heading level={3} colour="fancy">
                  News
                </Heading>
                <Text size="large">
                  Stay upto date on news and current affairs
                </Text>

                {products.news.map((news, i) => (
                  <div key={news.name}>
                    <hr />

                    <Input
                      type="checkbox"
                      name="news"
                      id={news.name}
                      label={news.name}
                      value={news.name}
                    />
                    <Text size="small">{news.description}</Text>

                    <Text size="xLarge" colour="fancy">
                      £{news.monthlyCost}{' '}
                      <Text size="small" element="span" colour="grey">
                        per month
                      </Text>
                    </Text>
                    <Text size="small">
                      Contract term {news.contractLength} months
                    </Text>

                    {i + 1 === products.news.length && <hr />}
                  </div>
                ))}
              </Card>
            </Grid>

            <Grid xs={12} sm={4}>
              <Card>
                <Heading level={3}>Basket</Heading>

                <Heading level={5}>Selected Sports channels</Heading>

                {basket.sports.length ? (
                  <ul>
                    {basket.sports.map(sport => (
                      <li key={sport}>
                        <strong data-test-id="basket-item">{sport}</strong>
                      </li>
                    ))}
                  </ul>
                ) : (
                  <Text size="small">
                    You haven&apos;t added any Sports channels to your basket
                  </Text>
                )}

                <hr />

                <Heading level={5}>Selected News channels</Heading>

                {basket.news.length ? (
                  <ul>
                    {basket.news.map(news => (
                      <li key={news}>
                        <strong data-test-id="basket-item">{news}</strong>
                      </li>
                    ))}
                  </ul>
                ) : (
                  <Text size="small">
                    You haven&apos;t added any News channels to your basket
                  </Text>
                )}

                <hr />

                <Heading level={5}>Basket total</Heading>

                <Text size="xLarge" colour="fancy" dataTestId="basket-total">
                  £{basket.total}{' '}
                  <Text size="small" element="span" colour="grey">
                    per month
                  </Text>
                </Text>

                <Button
                  isDisabled={!basket.news.length && !basket.sports.length}
                  shouldSubmit
                >
                  Checkout
                </Button>
              </Card>
            </Grid>
          </Grid>
        </Form>
      </Formik>
    </Section>
  )
}

export default HomePage
