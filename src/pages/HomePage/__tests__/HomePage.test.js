import React from 'react'

import mock from 'xhr-mock'
import { PRODUCTS } from '../../../api/products'

import data from '../../../../data/products.json'

import { render, fireEvent, waitFor } from '@testing-library/react'

import HomePage from '../HomePage'

describe('<HomePage>', () => {
  beforeEach(() => mock.setup())
  afterEach(() => mock.teardown())

  it('should get data from mock API and match snapshot', async () => {
    mock.get(PRODUCTS, {
      status: 200,
      body: data,
    })

    const { getByText, container, findByText } = render(<HomePage />)

    await findByText('Castcom F1')

    getByText('Build your Sky TV package')
    expect(container.firstChild).toMatchSnapshot()
  })

  it('should get data, add Castcom F1 to basket and calculate total', async () => {
    global.console.log = jest.fn()

    mock.get(PRODUCTS, {
      status: 200,
      body: data,
    })

    const { findByText, getByText } = render(<HomePage />)

    await findByText('Castcom F1')

    await waitFor(() => {
      fireEvent.click(getByText('', { selector: 'input[id="Castcom F1"]' }))
    })

    await waitFor(() => {
      fireEvent.click(
        getByText('', { selector: 'input[id="Castcom News HD"]' }),
      )
    })

    getByText('Castcom F1', { selector: '[data-test-id="basket-item"]' })
    getByText('Castcom News HD', { selector: '[data-test-id="basket-item"]' })

    // Cost of Castcom F1 + Castcom News HD should be 40
    getByText('£40', { selector: '[data-test-id="basket-total"]' })

    // Clicking on Checkout button should fire off a console log with the selected channels
    await waitFor(() => {
      fireEvent.click(getByText('Checkout', { selector: 'button' }))

      expect(global.console.log).toHaveBeenCalledWith({
        news: ['Castcom News HD'],
        sports: ['Castcom F1'],
      })
    })
  })
})
