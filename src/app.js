import React from 'react'

import '@babel/polyfill'

import ReactDOM from 'react-dom'

import { Router } from '@reach/router'

import HomePage from './pages/HomePage/HomePage'
import NotFoundPage from './pages/NotFoundPage/NotFoundPage'

import Header from './components/Header/Header'
import Footer from './components/Footer/Footer'

import './styles/index.scss'

export const App = () => (
  <>
    <Header />

    <main>
      <Router basepath="/app">
        <HomePage path="/" />
        <NotFoundPage default />
      </Router>
    </main>

    <Footer />
  </>
)

ReactDOM.render(<App />, document.getElementById('document'))

if (module.hot) {
  module.hot.accept()
}
